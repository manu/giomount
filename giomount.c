#include <string.h>
#include <gio/gio.h>

gboolean all = FALSE;
gboolean filter = FALSE;
gboolean listing = FALSE;
gboolean unmount = FALSE;
const gchar * const *args = NULL;

/* Item:
 *
 * A mountable volume. The @uuid and @name are owned by the structure. The
 * field @mount is non-%NULL if the volume is mounted. The field @selected is
 * used when filtering the list of available items.
 */

typedef struct {
	char *uuid;
	char *name;
	GVolume *volume;
	GMount *mount;
	gboolean selected;
} Item;

/**
 * volume_requested:
 * @uuid: the UUID of a volume
 * @name: the name of a volume
 *
 * Test if a volume should be retained when browsing volumes. When using an
 * external filter, all volumes are retained, it is the filter's
 * responsibility to do the selection.
 *
 * Returns: whether the volume should be selected
 */

gboolean
volume_requested (const char *uuid, const char *name)
{
	if (all || filter) return TRUE;
	if (args == NULL) return FALSE;

	const gchar * const *arg;
	for (arg = args; *arg; arg++)
		if (!g_strcmp0(*arg, uuid) || !g_strcmp0(*arg, name))
			return TRUE;
	return FALSE;
}

/**
 * browse_volumes:
 *
 * Produce a list of volumes, depending on the command-line parameters.
 *
 * Returns: a #GList of #Item structures
 */

GList*
browse_volumes ()
{
	GVolumeMonitor *monitor = g_volume_monitor_get();
	GList *list, *item, *selection = NULL;
	
	list = g_volume_monitor_get_volumes(monitor);
	for (item = list; item != NULL; item = item->next) {
		GVolume *volume = G_VOLUME(item->data);
		GMount *mount = NULL;
		char *name = g_volume_get_name(volume);
		char *uuid = g_volume_get_uuid(volume);

		g_debug("volume %s (%s)", uuid, name);

		GDrive *drive = g_volume_get_drive(volume);
		if (drive == NULL || !g_drive_is_removable(drive)) {
			g_debug("  not a removable drive");
			goto next;
		}
		
		mount = g_volume_get_mount(volume);

		if (unmount && mount == NULL) {
			g_debug("  not mounted");
			goto next;
		} else if (!unmount && mount != NULL) {
			g_debug("  mounted");
			goto next;
		}

		if (listing) {
			g_print("%s %s\n", uuid, name);
			goto next;
		}

		if (!volume_requested(uuid, name)) {
			g_debug("  not requested");
			goto next;
		}

		g_debug("  requested");
		Item *item = g_new(Item, 1);
		item->uuid = g_strdup(uuid);
		item->name = g_strdup(name);
		item->volume = g_object_ref(volume);
		item->mount = mount == NULL ? NULL : g_object_ref(mount);
		item->selected = TRUE;
		selection = g_list_append(selection, item);
	next:
		g_clear_object(&mount);
		g_clear_object(&drive);
		g_free(name);
		g_free(uuid);
	}
	g_list_free_full(list, g_object_unref);

	return selection;
}

/**
 * filter_selection:
 * @selection: the list of items to select in
 * @error: a #GError
 *
 * Run the external filter for selection in a list of items.
 *
 * Returns: %TRUE on success
 */

gboolean
filter_selection (GList *selection, GError **error)
{
	gboolean result = FALSE;
	GOutputStream *input = NULL;
	GInputStream *output = NULL;
	GDataInputStream *data = NULL;
	char *line = NULL;
	gsize length;

	/* Spawn the filter process. */

	GSubprocess *filter = g_subprocess_newv(args,
		G_SUBPROCESS_FLAGS_STDIN_PIPE | G_SUBPROCESS_FLAGS_STDOUT_PIPE,
		error);
	if (filter == NULL) goto end;

	/* Send the list of volumes as input data. */

	input = g_subprocess_get_stdin_pipe(filter);
	for (GList *node = selection; node != NULL; node = node->next) {
		Item *item = node->data;
		if (!g_output_stream_printf(input, NULL, NULL, error,
				"%s %s\n", item->uuid, item->name))
			goto end;

		item->selected = FALSE;
	}
	if (!g_output_stream_close(input, NULL, error)) goto end;

	/* Parse the output lines. */

	output = g_subprocess_get_stdout_pipe(filter);
	data = g_data_input_stream_new(output);
	while ((line = g_data_input_stream_read_line(data, &length, NULL, error)) != NULL) {
		g_debug("line (%ld): %s", (long)length, line);
		if (length < 9) {
			g_clear_pointer(&line, g_free);
			continue;
		}
		for (GList *node = selection; node != NULL; node = node->next) {
			Item *item = node->data;
			if (!strncmp(line, item->uuid, 9)) {
				g_debug("select %s", item->uuid);
				item->selected = TRUE;
				break;
			}
		}
		g_clear_pointer(&line, g_free);
	}
	if (error != NULL && *error != NULL) goto end;

	if (!g_subprocess_wait_check(filter, NULL, error)) goto end;
	result = TRUE;
end:
	if (line != NULL) g_free(line);
	g_clear_object(&data);
	g_clear_object(&filter);
	return result;
}

/**
 * State:
 *
 * The state of th munting process.
 */

typedef struct {
	GMainLoop *main_loop;
	int pending;
} State;

/**
 * handle_selection:
 * @item: a pointer to an #Item structure
 * @state: a pointer to a #State
 *
 * Perform the requested action on an item. Mounts and unmounts are
 * asynchronous. When all actions are actually performed, exit the main loop.
 */

void
when_unmounted (GObject *mount, GAsyncResult *res, gpointer data)
{
	GError *error = NULL;
	State *state = data;
	char *uuid = g_mount_get_uuid(G_MOUNT(mount));

	if (!g_mount_unmount_with_operation_finish(G_MOUNT(mount), res, &error))
		g_printerr("error unmounting %s: %s\n", uuid, error->message);
	else
		g_debug("unmounted %s", uuid);

	g_free(uuid);

	if (--state->pending == 0) g_main_loop_quit(state->main_loop);
}

void
when_mounted (GObject *volume, GAsyncResult *res, gpointer data)
{
	GError *error = NULL;
	State *state = data;
	char *uuid = g_volume_get_uuid(G_VOLUME(volume));

	if (!g_volume_mount_finish(G_VOLUME(volume), res, &error))
		g_printerr("error mounting %s: %s\n", uuid, error->message);
	else
		g_debug("mounted %s", uuid);

	g_free(uuid);

	if (--state->pending == 0) g_main_loop_quit(state->main_loop);
}

void
handle_selection (gpointer item, gpointer data)
{
	Item *it = item;
	State *state = data;

	if (!it->selected) {
		g_debug("not selected: %s (%s)", it->uuid, it->name);
		return;
	}

	if (unmount) {
		g_debug("unmounting %s (%s)", it->uuid, it->name);
		g_mount_unmount_with_operation(it->mount,
			G_MOUNT_UNMOUNT_NONE,
			NULL, NULL,
			when_unmounted, state);
	} else {
		g_debug("mounting %s (%s)", it->uuid, it->name);
		g_volume_mount(it->volume,
			G_MOUNT_MOUNT_NONE,
			NULL, NULL,
			when_mounted, state);
	}
	state->pending++;
}

/**
 * main:
 * @argc: the array of comand-line arguments
 * @argv: the size of @argc
 *
 * The entry point of the program.
 *
 * Returns: the exit code
 */

GOptionEntry option_entries[] = {
	{ "all", 'a', 0, G_OPTION_ARG_NONE, &all,
		"(Un)mount all volumes", NULL },
	{ "filter", 'f', 0, G_OPTION_ARG_NONE, &filter,
		"Filter the list of volumes with an external command", NULL },
	{ "list", 'l', 0, G_OPTION_ARG_NONE, &listing,
		"List available volumes", NULL },
	{ "unmount", 'u', 0, G_OPTION_ARG_NONE, &unmount,
		"Unmount volumes", NULL },
	{ G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_STRING_ARRAY, &args,
		"Volume names or filter command", NULL },
	{ NULL }
};

int
main (int argc, char **argv)
{
	GError *error = NULL;
	GOptionContext *context;

	/* Parse the command line */

	context = g_option_context_new("[ID...|CMD...]");
	g_option_context_set_summary(context,
		"Positional arguments:\n"
		"  ID                UUID or names of volumes to (un)mount\n"
		"  CMD               External filter command (when using -f)");
	g_option_context_add_main_entries(context, option_entries, NULL);
	if (!g_option_context_parse(context, &argc, &argv, &error)) {
		g_print("%s\n", error->message);
		return 1;
	}

	/* Browse the volumes */

	GList *selection = browse_volumes();

	/* Filter the selection, if relevant. */

	if (filter && g_list_length(selection) > 1) {
		GError *error = NULL;
		if (!filter_selection(selection, &error)) {
			g_print("Error: %s\n", error->message);
			g_clear_error(&error);
			return 1;
		}
	}

	if (selection == NULL) {
		g_debug("no volume selected");
		return 0;
	}

	/* Perform mounting operations. */

	State state = { g_main_loop_new(NULL, FALSE), 0 };
	g_list_foreach(selection, handle_selection, &state);
	if (state.pending > 0)
		g_main_loop_run(state.main_loop);

	return 0;
}
