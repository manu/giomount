all: giomount

include config.mk
config.mk:
	./configure

install: $(PREFIX)/bin/giomount

$(PREFIX)/bin/%: %
	mkdir -p $(PREFIX)/bin
	install -m 755 $< $@

giomount: giomount.c
	$(CC) $(CFLAGS) $$(pkg-config --cflags --libs gio-2.0) $< -o $@

.PHONY: all install
